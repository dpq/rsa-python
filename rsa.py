import random
import binascii

verbose = False


def check_prime(number):
	# todo what is a good k?
	k = 50
	for _ in range(k + 1):
		a = random.randint(2, number - 2)
		f = pow(a, number-1, number)
		if f != 1:
			return False

	# also check that e == 17 is not coprime with either p-1 or q-1
	if (number - 1) % 17 == 0:
		return False
	return True


def extended_euclidean(a, b):
	if b == 0:
		return a, 1, 0
	g_prime, x_prime, y_prime = extended_euclidean(b, a % b)
	return g_prime, y_prime, x_prime - y_prime * (a // b)


def encrypt(e, n):
	print("Write your message")
	msg_str = input()
	msg_int = int(binascii.hexlify(msg_str.encode("utf-8")), 16)
	if msg_int > n:
		print("Message is too long. Try with a smaller message")
		return False
	return pow(msg_int, e, n)


def decrypt(d, n):
	print("Write your ciphertext")
	ciphertext = int(input())
	msg_int = pow(ciphertext, d, n)
	try:
		msg_str = binascii.unhexlify(hex(msg_int)[2:]).decode("utf-8")
	except:
		return "Not ciphertext of utf-8"
	return msg_str


def main():
	p, q = 4, 4
	i = 0
	while not check_prime(p):
		if verbose and i % 100 == 0:
			print("First prime generation attempt ", i)
		i += 1
		p = random.randint(2**2047, 2**2048)
	i = 0
	while not check_prime(q):
		if verbose and i % 100 == 0:
			print("Second prime generation attempt ", i)
		i += 1
		q = random.randint(2**2047, 2**2048)

	n = p * q
	phi = (p - 1) * (q - 1)
	e = 17  # todo, understand why e normally is 17

	gcd, x, y = extended_euclidean(phi, e)
	d = y % phi
	print("The public keys are\n"
		  "n = ", n, "\n"
		  "e = ", e)
	if verbose:
		print("p = ", p, "\n",
			  "q = ", q, "\n",
			  "d = ", d, "\n",
			  "phi = ", phi, "\n",
			  "x*phi + y*e = d\n",
			  "x = ", x, "\n",
			  "y = ", y, "\n"
			  )
	assert (d * e) % phi == 1
	while True:
		print("What would you like to do?\n"
			  "(e)ncrypt\n"
			  "(d)ecrypt\n"
			  "(q)uit")
		action = input()
		if action == "e":
			ciphertext = encrypt(e, n)
			if ciphertext:
				print("Your ciphertext is:\n", ciphertext)
		if action == "d":
			plaintext = decrypt(d, n)
			print("Your plaintext is:\n", plaintext)
		if action == "q":
			return 0


if __name__ == "__main__":
	main()


